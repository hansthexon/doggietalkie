﻿using SQLite4Unity3d;
public class Code{

	[PrimaryKey,AutoIncrement,Unique]
	public string CodeARimage { get; set; }
	public string  VideoFile { get; set; }
	public string 	Type { get; set; }
	public string Type2 { get; set; }
	public string PolitePlanet { get; set; }
	public string SelfVideoFile { get; set; }
	public string Country { get; set; }
	public string Purchased { get; set; }
	public string Content { get; set; }
	public int FirstScore { get; set; }
	public int RescanScore { get; set; }
	public int SelfFirstScore { get; set; }
	public int SelfRescanScore { get; set; }
	public int Version { get; set; }
	public string ForMonth { get; set; }
	public string First { get; set; }
	public string 	SelfFirst { get; set; }

	public string Location { get; set; }

	public string PoliteFirst { get; set; }




}

