﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Vuforia;
using System;

public class GameManager1 : MonoBehaviour {

	public bool buyfreebool;
	public Toggle freecode_toggle;
	public Toggle buythree_toggle;
	public GameObject notfound;
	public GameObject videoPrefab;
	public GameObject[] DisableMenu;

	public Animator Dog;
	public UnityEngine.UI.Image unlockedImage;
	public UnityEngine.UI.Image[] roomobjects;

	public GameObject CongratsPanel;
	public 	UnityEngine.UI.Image starbar;
	RoomObject robjects;
	public GameObject ArPanel;
	public GameObject StarBracket;
	public GameObject PlanetBracket;

	public GameObject ReplayPanel;
	int currentscore;


	public GameObject PlanetEarnoints;
	public GameObject StarEarnPoints;

	public GameObject PlanetInScorePanel;
	public GameObject CoininScorePanel;



	public GameObject FreeCodeScreen;

	public GameObject ThreeCodeScreen;

	public GameObject ParentCorner;

	public GameObject SlideE;
	public GameObject SlideC;

	public Text CurrentScoretext;
	public	Text StarScoreText;
	public Text PlanerScoreText;
	string StarScore="StarScore";
	string PlanetScore="PlanetScore";

	public	bool cantrack=false;


	public HandController hand;
	bool politeplanet=true;
	//	public GameObject MainCam;
	public VuforiaBehaviour ArCam;
	public GameObject videoManager;
	Code codeobject;
//	public GameObject videoprefab;
	public MediaPlayerCtrl player;
	public	string tempfilename;
	//public RawImage videoscreen;
	public 	GameObject mainpanel;

	public GameObject Imagsets;

	public string filepath;
	DataService ds;
	public GameObject Scorepanel;




	public GameObject politeLoadingScreen;

	public GameObject PolitePlanet;

	int i=0;
	// Use this for initialization
	void Start () {
		Dog = Dog.GetComponent<Animator> ();
		unlockedImage=unlockedImage.GetComponent<UnityEngine.UI.Image> ();
		starbar=starbar.GetComponent<UnityEngine.UI.Image> ();

	//	player=	videoManager.GetComponent<MediaPlayerCtrl> ();

		ArCam =	ArCam.GetComponent<VuforiaBehaviour> ();
		ds = new DataService ("arapp.sqlite");
	
		CurrentScoretext = CurrentScoretext.GetComponent<Text> ();
		StarScoreText = StarScoreText.GetComponent<Text> ();
		PlanerScoreText = PlanerScoreText.GetComponent<Text> ();

		StarScoreText.text=PlayerPrefs.GetInt(StarScore).ToString();
		PlanerScoreText.text=PlayerPrefs.GetInt(PlanetScore).ToString();
		showRoomObjects ();
		UpdateBar ();
		//	player=videoManager.GetComponent<MediaPlayerCtrl> ();
		//	player.OnEnd += SetScore;
	}

	// Update is called once per frame
	void Update () {

	}



	public void OpenArCam(){
		
		ArCam.enabled = true;
		cantrack = true;
		politeplanet = true;
		//MainCam.SetActive (false);
		//	ArCam.SetActive (true);
		Imagsets.SetActive (true);
		mainpanel.SetActive (false);

		StarBracket.SetActive (false);
		PlanetBracket.SetActive (false);

		OpenArPanel ();
	}

void	OpenArPanel(){

		hand.EnableHand ();
		ArPanel.SetActive (true);
		//play animation two times




	}

	public 	void CloseARPanel(){
		cantrack = false;
		mainpanel.SetActive (true);
	    StarBracket.SetActive (true);
		PlanetBracket.SetActive (true);
		ArPanel.SetActive (false);
	}
	// called by imagetargetclass and it will provide the name of the file found
	public void ObjectFound(string filename){

	

		fetchInfoFromDb (filename);
		//fetch all records related tothis clip
		//check if file has been purchased

		Debug.Log (codeobject.Purchased);
		if (codeobject.Purchased == "Y") {

			ArCam.enabled = false;
			ArPanel.SetActive (false);
			politeplanet = true;

			if (codeobject.Location == "I") {

				filepath = filename + ".mp4";
			} else {
				filepath = "file://" + Application.persistentDataPath + "/" + filename + ".mp4";

			}

			playvideo ();

			player.OnEnd += SetScore;
			//


			//MainCam.SetActive (true);
			//	ArCam.SetActive(false);


			//Imagsets.SetActive (false);   // imagesets


			//

		} else {
			
			notfound.SetActive (true);
		}

	}

	void fetchInfoFromDb(string filename){

		codeobject=ds.getVideo(filename);


	}

	void SetScore(){


		//videoscreen.gameObject.SetActive (false);
		player.OnEnd -= SetScore;
		player.UnLoad ();
		//not this one

		Scorepanel.SetActive (true);
		CoininScorePanel.SetActive (true);
		StarEarnPoints.SetActive (true);
		SaveScore (true);



	}


	void SetPlanetScore(){
		//update score
		politeplanet=false;
	//	videoscreen.gameObject.SetActive (false);
		player.OnEnd -= SetPlanetScore;
		player.UnLoad ();

		Scorepanel.SetActive (true);
		PlanetInScorePanel.SetActive (true);
		PlanetEarnoints.SetActive (true);
		SaveScore (false);


	}

	public 	void Setmyscore(){
		//videoscreen.gameObject.SetActive (false);



		player.OnEnd -= SetScore;
		Destroy (videoManager);


		Scorepanel.SetActive (true);
		//mainpanel.SetActive (true);


	}




	public void backButton(){

		if(codeobject.PolitePlanet=="Y"&&politeplanet){
			///if we keep files not in streaming assets
			/// if internal
			/// 
			/// 



			Debug.Log("Polite Planet on back"+codeobject.SelfVideoFile+".mp4");


		
			if (codeobject.Location == "I") {
				filepath = codeobject.SelfVideoFile + ".mp4";

			} else {


				filepath="file://"+Application.persistentDataPath+"/"+codeobject.SelfVideoFile+".mp4";

			}
		//	videoscreen.gameObject.SetActive (false);

			player.Load(filepath);
			player.OnReady += playy;
			player.OnEnd += SetPlanetScore;



		}else{

			Destroy (videoManager);
			//videoscreen.gameObject.SetActive (false);
			ReplayPanel.SetActive (false);
			mainpanel.SetActive (true);
			PlanetBracket.SetActive (true);
			StarBracket.SetActive (true);

			if (PlayerPrefs.GetInt ("five") > 500) {


				Dog.SetTrigger ("jump");


				PlayerPrefs.SetInt ("five", 0);

			}

			//cantrack = true;

		}


	}


	public void Replay(){
		//videoscreen.gameObject.SetActive (false);
	

		//Scorepanel.SetActive (false);
		player.Load (filepath);
		player.OnReady += playy;
		if (politeplanet) {

		
			player.OnEnd += SetScore;


		} else {
			
			player.OnEnd += SetPlanetScore;

		}
	}
	void playvideo(){
		
		//	videoscreen.gameObject.SetActive (true);

		videoManager=	Instantiate(videoPrefab) as GameObject;
		player = videoManager.GetComponent<MediaPlayerCtrl> ();
		//	videoManager.SetActive (true);
		player.Load (filepath);

		player.OnReady += playy;

	}



	bool isSelfFirst(){

		return codeobject.SelfFirst == "Y";
	}


	bool isFirst(){

		return codeobject.First == "Y";
	}


	void SaveScore(bool isStar){


		PlanetBracket.SetActive (true);
		StarBracket.SetActive (true);

		if (isStar) {
			if (codeobject.First == "Y") {

				PlayerPrefs.SetInt (StarScore,PlayerPrefs.GetInt (StarScore)+ codeobject.FirstScore);
				currentscore = codeobject.FirstScore;
				codeobject.First = "N";
				ds.UpdateFirst (codeobject);


				SetFive (codeobject.FirstScore);


			} else {
				PlayerPrefs.SetInt (StarScore,PlayerPrefs.GetInt (StarScore)+codeobject.RescanScore);
				currentscore	= codeobject.RescanScore;
				SetFive (codeobject.FirstScore);
			}


		} else {

			if (codeobject.SelfFirst == "Y") {

				PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfFirstScore);
				currentscore = codeobject.SelfFirstScore;
				codeobject.SelfFirst = "N";
				ds.UpdateFirst (codeobject);

			} else {
				PlayerPrefs.SetInt (PlanetScore,PlayerPrefs.GetInt (PlanetScore)+codeobject.SelfRescanScore);

				currentscore = codeobject.SelfRescanScore;



			}

		}

		//have we unclocked something 


		if (PlayerPrefs.GetInt (StarScore) > Int32.Parse (robjects.ReachScore)) {

			//unlock that object 


			robjects.Earned = "Y";
			ds.UpdateRoomObjects (robjects);


			///play some animation 
			// get image from 
		
			StartCoroutine (ShowConratsScreen ());
		} else {
	




			UpdateDisplay ();
			StartCoroutine (ShowReplay ());
		}



	}




	IEnumerator ShowReplay(){


		yield return new WaitForSeconds(2);
		PlanetInScorePanel.SetActive (false);
		CoininScorePanel.SetActive (false);
		StarEarnPoints.SetActive (false);
		PlanetEarnoints.SetActive (false);
		PlanetBracket.SetActive (false);
		StarBracket.SetActive (false);
		Scorepanel.SetActive (false);
		ReplayPanel.SetActive (true);

	}



	void UpdateDisplay(){
		UpdateBar ();

		PlanerScoreText.text=	PlayerPrefs.GetInt (PlanetScore).ToString();
		CurrentScoretext.text = "X"+currentscore.ToString ();

	}


	void UpdateBar(){


		//PlayerPrefs.SetInt (StarScore, 400);
		GetCurrentObjective ();
		starbar.fillAmount = (float)PlayerPrefs.GetInt (StarScore) / Int32.Parse(robjects.ReachScore);
		StarScoreText.text=PlayerPrefs.GetInt (StarScore).ToString()+"/"+Int32.Parse(robjects.ReachScore);




	}

	void GetCurrentObjective(){


				robjects=ds.GetCurrentUnclocked ();




	}

	IEnumerator  ShowConratsScreen(){


		yield return new  WaitForSeconds(2);
		ReplayPanel.SetActive (false);

		CongratsPanel.SetActive (true);
		unlockedImage.sprite = getmysprite (robjects.ObjPNG);
	//	unlockedImage.preserveAspect = true;




	}
	public void Congratsback(){
		Destroy (videoManager);
		mainpanel.SetActive (true);
		PlanetBracket.SetActive (true);
		StarBracket.SetActive (true);
		CongratsPanel.SetActive (false);
		showRoomObjects ();
		UpdateBar ();
		Dog.SetTrigger ("sit");


	}


	Sprite getmysprite(string name){
		for(int i=0; i<=roomobjects.Length;i++) {
			

			if (roomobjects [i].name == name) {

				return roomobjects [i].sprite;

			}



		

			}

		return null;
	}

	void showRoomObjects(){


		var unlocked = ds.GetUnclocked ();


		foreach (RoomObject rooms in unlocked) {

			foreach (UnityEngine.UI.Image img in roomobjects) {

				if (img.name == rooms.ObjPNG) {


					img.gameObject.SetActive (true);


				}

			}

		}

	}



	void 	playy(){
		
		StarBracket.SetActive (false);
		PlanetBracket. SetActive (false);

		player.Play ();

		foreach (GameObject panles in DisableMenu) {

			panles.SetActive (false);

		}
	}

	public void SetFive(int five){

		PlayerPrefs.SetInt ("five", PlayerPrefs.GetInt ("five") + five);


	}




	public void close(){
		
		notfound.SetActive (false);
		cantrack = true;
	}



	public void OpenParentCorner(){

		ParentCorner.SetActive (true);
	}

	public void closeParemt(){

		ParentCorner.SetActive (false);
	}


	public void OpenPurchaseScreen(){

		if (PlayerPrefs.GetString ("reg", "no") == "yes") {

			ThreeCodeScreen.SetActive (true);


		} else {


			SlideC.SetActive (true);

		}



	}


	public void buyfreescode(bool b){
		if (b) {
			Debug.Log ("B");

			buythree_toggle.isOn = false;

			buyfreebool = true;
		} else {
			buyfreebool = false;

			buythree_toggle.isOn = true;



		}

	} 

	public void buyletsthree(bool c){

		Debug.Log ("C");
		if (c) {
			buyfreebool = false;
			freecode_toggle.isOn = false;
			//buythree_toggle.isOn = true;


		} else {
			buyfreebool = true;

			freecode_toggle.isOn = true;


		}

	}



	public void three_or_singlebuy(){
		if (buyfreebool) {



			SlideE.SetActive (true);
		} else {
			ThreeCodeScreen.SetActive (true);

		}

	}


	public void OpenPolitePlanet(){


		StartCoroutine (ploiteloadingscreen ());

	}

	IEnumerator ploiteloadingscreen(){

		politeLoadingScreen.SetActive (true);
		yield	return new WaitForSeconds (2);
		politeLoadingScreen.SetActive (false);

		PolitePlanet.SetActive (true);

	}







}
