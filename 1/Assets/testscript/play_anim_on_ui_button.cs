﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class play_anim_on_ui_button : MonoBehaviour 
{
	
	public Button Text;
	public AudioClip sound;
	public Animator ani;
	public Canvas yourcanvas;
	
	
	
	void Start123 () 
	{
		Text = Text.GetComponent<Button> ();
		ani.enabled = false;
		yourcanvas.enabled = true;
	}

	
	public void Press123() 
		
	{
		Text.enabled = true;
		AudioSource.PlayClipAtPoint(sound, transform.position);
		ani.enabled = true;
		Destroy(Text,1);
		yourcanvas.enabled = false;

	}
}
