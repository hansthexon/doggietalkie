using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AttackActions : MonoBehaviour {
	public GameObject Ninjahand;
	Animator ninjaanim;
	// Use this for initialization
	void Start () {
		ninjaanim = GetComponent<Animator> ();
	}

	public void EnableNinjahand(){
		Ninjahand.SetActive (true);
	}

	public void DisableNinjahand(){
		Ninjahand.SetActive (false);
	}

	public void Attack(){
		ninjaanim.SetTrigger ("attack4");
		DisableNinjahand ();
	}
}
